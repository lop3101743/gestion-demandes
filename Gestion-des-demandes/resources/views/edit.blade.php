<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modifier la Demande</title>
</head>
<body>
    <h1>Modifier la Demande</h1>

    <form action="{{ route('demand.update', $demand->id) }}" method="POST">
        @csrf
        @method('PUT')
        
        <label for="title">Titre :</label>
        <input type="text" id="title" name="title" value="{{ $demand->title }}"><br><br>
        
        <label for="description">Description :</label><br>
        <textarea id="description" name="description">{{ $demand->description }}</textarea><br><br>
        
        <label for="status">Statut :</label>
        <select id="status" name="status">
            <option value="Nouveau" {{ $demand->status == 'Nouveau' ? 'selected' : '' }}>Nouveau</option>
            <option value="En cours" {{ $demand->status == 'En cours' ? 'selected' : '' }}>En cours</option>
            <option value="Terminé" {{ $demand->status == 'Terminé' ? 'selected' : '' }}>Terminé</option>
        </select><br><br>
        
        <!-- Ajoutez d'autres champs au besoin -->
        
        <button type="submit">Enregistrer</button>
    </form>
</body>
</html>
