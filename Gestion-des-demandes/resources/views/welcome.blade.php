<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome</title>
</head>
<body>
    <h1>Bienvenue sur votre application de gestion des demandes pour une société d'architecture</h1>

    <h2>Accès rapide aux fonctionnalités :</h2>
    <ul>
        <li><a href='/demand.index'>Liste des demandes</a></li>
        <li><a href='/demand.create'>Créer une demande</a></li>
        <li><a href='/architect.index'>Liste des architectes</a></li>
        <li><a href='/architect.create'>Ajouter un architecte</a></li>
        <li><a href='/comment.index'>Liste des commentaires</a></li>
        <li><a href='/comment.create' >Ajouter un commentaire</a></li>
    </ul>
</body>
</html>
