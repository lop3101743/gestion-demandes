<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Liste des Demandes</title>
</head>
<body>
    <h1>Liste des Demandes</h1>

    <a href="{{ route('demand.create') }}">Créer une nouvelle demande</a>

    <ul>
        @foreach($demands as $demand)
            <li>{{ $demand->title }} - {{ $demand->description }}</li>
        @endforeach
    </ul>
</body>
</html>
