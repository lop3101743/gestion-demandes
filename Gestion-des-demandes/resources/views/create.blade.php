<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Créer une Nouvelle Demande</title>
</head>
<body>
    <h1>Créer une Nouvelle Demande</h1>

    <form action="{{ route('demand.store') }}" method="POST">
        @csrf
        <label for="title">Titre :</label>
        <input type="text" id="title" name="title"><br><br>
        
        <label for="description">Description :</label><br>
        <textarea id="description" name="description"></textarea><br><br>
        
        <label for="status">Statut :</label>
        <select id="status" name="status">
            <option value="Nouveau">Nouveau</option>
            <option value="En cours">En cours</option>
            <option value="Terminé">Terminé</option>
        </select><br><br>
        
        <!-- Ajoutez d'autres champs au besoin -->
        
        <button type="submit">Enregistrer</button>
    </form>
</body>
</html>
