<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Demand;

class DemandController extends Controller
{
    public function index()
    {
        $demands = Demand::all();
        return view('demand.index', compact('demands'));
    }

    public function create()
    {
        return view('demand.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'status' => 'required',
        ]);

        Demand::create($request->all());

        return redirect()->route('demand.index')
                         ->with('success', 'Demande créée avec succès.');
    }

    public function show($id)
    {
        $demand = Demand::findOrFail($id);
        return view('demand.show', compact('demand'));
    }

    public function edit($id)
    {
        $demand = Demand::findOrFail($id);
        return view('demand.edit', compact('demand'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'status' => 'required',
        ]);

        $demand = Demand::findOrFail($id);
        $demand->update($request->all());

        return redirect()->route('demand.index')
                         ->with('success', 'Demande mise à jour avec succès.');
    }

    public function destroy($id)
    {
        $demand = Demand::findOrFail($id);
        $demand->delete();

        return redirect()->route('demand.index')
                         ->with('success', 'Demande supprimée avec succès.');
    }
}
