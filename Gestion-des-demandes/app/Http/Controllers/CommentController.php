<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends Controller
{
    public function index()
    {
        $comments = Comment::all();
        return view('comment.index', compact('comments'));
    }

    public function create()
    {
        return view('comment.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'content' => 'required',
            'user_id' => 'required',
            'demand_id' => 'required',
        ]);

        Comment::create($request->all());

        return redirect()->route('comment.index')
                         ->with('success', 'Commentaire créé avec succès.');
    }

    public function show($id)
    {
        $comment = Comment::findOrFail($id);
        return view('comment.show', compact('comment'));
    }

    public function edit($id)
    {
        $comment = Comment::findOrFail($id);
        return view('comment.edit', compact('comment'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'content' => 'required',
            'user_id' => 'required',
            'demand_id' => 'required',
        ]);

        $comment = Comment::findOrFail($id);
        $comment->update($request->all());

        return redirect()->route('comment.index')
                         ->with('success', 'Commentaire mis à jour avec succès.');
    }

    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);
        $comment->delete();

        return redirect()->route('comment.index')
                         ->with('success', 'Commentaire supprimé avec succès.');
    }
}
