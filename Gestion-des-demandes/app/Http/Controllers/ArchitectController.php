<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Architect;

class ArchitectController extends Controller
{
    public function index()
    {
        $architects = Architect::all();
        return view('architect.index', compact('architects'));
    }

    public function create()
    {
        return view('architect.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:architects',
        ]);

        Architect::create($request->all());

        return redirect()->route('architect.index')
                         ->with('success', 'Architecte créé avec succès.');
    }

    public function show($id)
    {
        $architect = Architect::findOrFail($id);
        return view('architect.show', compact('architect'));
    }

    public function edit($id)
    {
        $architect = Architect::findOrFail($id);
        return view('architect.edit', compact('architect'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:architects,email,'.$id,
        ]);

        $architect = Architect::findOrFail($id);
        $architect->update($request->all());

        return redirect()->route('architect.index')
                         ->with('success', 'Architecte mis à jour avec succès.');
    }

    public function destroy($id)
    {
        $architect = Architect::findOrFail($id);
        $architect->delete();

        return redirect()->route('architect.index')
                         ->with('success', 'Architecte supprimé avec succès.');
    }
}
