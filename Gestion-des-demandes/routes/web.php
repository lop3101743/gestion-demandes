<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DemandController;
use App\Http\Controllers\ArchitectController;
use App\Http\Controllers\CommentController;

// Routes pour les demandes
Route::get('/demands', [DemandController::class, 'index'])->name('demand.index');
Route::get('/demands/create', [DemandController::class, 'create'])->name('demand.create');

// Routes pour les architectes
Route::get('/architects', [ArchitectController::class, 'index'])->name('architect.index');
Route::get('/architects/create', [ArchitectController::class, 'create'])->name('architect.create');

// Routes pour les commentaires
Route::get('/comments', [CommentController::class, 'index'])->name('comment.index');
Route::get('/comments/create', [CommentController::class, 'create'])->name('comment.create');

